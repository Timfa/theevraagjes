#!/bin/sh

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Check if hunspell command exists
if ! command_exists hunspell; then
    echo "Error: hunspell command not found. Attempting to install..."

    # Install Hunspell and Dutch dictionary (adjust based on your OS package manager)
    if command_exists apt-get; then
        apt-get update
        apt-get install -y hunspell hunspell-nl
    elif command_exists yum; then
        yum install -y hunspell hunspell-nl
    elif command_exists apk; then
        apk add hunspell hunspell-nl
    else
        echo "Error: Unable to install hunspell. Please install manually and ensure it's in your PATH."
        exit 1
    fi
fi

# Define the file to check
file_to_check="web/vragen.php"

# Variable to track errors
errors_found=0

# Function to check spelling and grammar using Hunspell
check_spelling_grammar() {
    local file="$1"
    local local_errors_found=0

    # Extract text enclosed in double quotes and check each sentence
    while IFS= read -r text; do
        # Skip empty lines
        if [ -z "$text" ]; then
            continue
        fi

        local errors=0

        # Check punctuation (ends with '.', '?', or '!')
        if ! echo "$text" | grep -qE '[.!?]$'; then
            echo "\033[0;31mPunctuation error in sentence \"$text\"\033[0m"
            errors=1
        fi

        # Spell and grammar check using Hunspell
        hunspell_output=$(echo "$text" | hunspell -l -d nl)
        
        # Check if Hunspell found any errors
        if [ -n "$hunspell_output" ]; then
            echo -e "\033[0;31mErrors found in sentence \"$text\"\033[0m"
            echo -e "\033[0;31mErrors:\033[0m"
            echo -e "\033[0;31m  $hunspell_output\033[0m"
            errors=1
        fi

        # Color the output based on whether the sentence passed or failed
        if [ $errors -eq 0 ]; then
            echo -e "\033[0;32mString \"$text\" passed all checks.\033[0m"
        else
            echo -e "\033[0;31mString \"$text\" failed one or more checks.\033[0m"
            local_errors_found=1
        fi
    done < <(sed -n 's/.*"\(.*\)".*/\1/p' "$file")

    # Accumulate errors found in local_errors_found to errors_found
    if [ $local_errors_found -eq 1 ]; then
        errors_found=1
    fi
}

# Call the function with your file to check
check_spelling_grammar "$file_to_check"

# Exit with non-zero status if errors were found
if [ $errors_found -eq 1 ]; then
    echo -e "\033[0;31mGrammar and spelling checks failed.\033[0m"
    exit 1
else
    echo -e "\033[0;32mGrammar and spelling checks passed.\033[0m"
    exit 0
fi
