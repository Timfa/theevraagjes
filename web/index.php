<?php

include("vragen.php");

header("Content-type: image/png");
$string = Vraag();

$strsplit = explode(" " ,$string);
$counter = 0;

for($i = 0; $i < count($strsplit); $i++)
{
    $counter += strlen($strsplit[$i]);

    if($i < count($strsplit)-1)
    {
        if($counter + strlen($strsplit[$i + 1]) >= 20)
        {
            $counter = 0;
            $strsplit[$i] .= "\n";
        }
        else 
        {
            $counter++;
            $strsplit[$i] .= " ";
        }
    }
}

$string = implode($strsplit);

$strs = explode("\n" ,$string);

$im = null;

if($string == "grinnik.")
{
    $im     = imagecreatefrompng("leeg_grinnik.png");
}
else
{
    $im     = imagecreatefrompng("leeg_s.png");
    $orange = imagecolorallocate($im, 255, 0, 6);

    for($i = 0; $i < count($strs); $i++)
    {
        $px     = (imagesx($im) - 7.2 * strlen($strs[$i])) / 2;
        $py     = 12 + (imagesy($im) / 2) - ((count($strs)/2) - $i) * 20;
        imagestring($im, 3, $px, $py, $strs[$i], $orange);
    }
}

echo imagepng($im);
imagedestroy($im);